# Dockerfile,

FROM openjdk:21
RUN mkdir /usr/app/ 
COPY target/Testudinidae_App.jar /usr/app 
WORKDIR /usr/app/   
ENTRYPOINT [ "java","-jar","Testudinidae_App.jar" ]