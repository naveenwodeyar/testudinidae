package com.bhas.entity;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PassengerInfo 
{
	private Long pId;
	private String pName;
	private String pMail;
	private String pAddress;
}
