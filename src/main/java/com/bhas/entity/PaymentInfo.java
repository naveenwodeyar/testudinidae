package com.bhas.entity;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaymentInfo 
{
	private Long pmtId;
	private String pmtType; 
}
