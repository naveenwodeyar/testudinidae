package com.bhas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestudinidaeApplication 
{

	public static void main(String[] args) 
	{
		SpringApplication.run(TestudinidaeApplication.class, args);
		System.out.println("\nPatience is bitter, but its fruit is sweet.");
	}

}
